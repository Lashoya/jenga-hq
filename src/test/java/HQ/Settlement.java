package HQ;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

public class Settlement extends LoginDriver {
	
	@Test
	
	public static void Settlement() throws InterruptedException {
		System.out.println("user logged in successfully");
				
		driver.manage().window().maximize();
			
		Thread.sleep(2000);
		
		WebElement settlement = driver.findElement(By.xpath("/html/body/div[1]/div[3]/div[1]/div/div/ul/li[5]/a"));
		//WebElement settlement = driver.findElement(By.tagName("Settlement"));
		settlement.click();
		
		WebElement settlementamount = driver.findElement(By.id("SettlementAmount"));
		settlementamount.sendKeys("101");
		
		Thread.sleep(1000);
		
		
		//Select currency = new Select(driver.findElement(By.id("AccountCurrency")));
		Select currency = new Select(driver.findElement(By.xpath("/html/body/div[1]/div[3]/div[2]/div/div/div[3]/div/div/div[2]/table/tbody/tr/td[2]/select")));
		currency.selectByValue("KES");
		
		Select paymentmethod = new Select(driver.findElement(By.xpath("/html/body/div[1]/div[3]/div[2]/div/div/div[3]/div/div/div[2]/table/tbody/tr/td[3]/select")));
		paymentmethod.selectByValue("Equity, 1100161758939");
		
		WebElement completesettlement = driver.findElement(By.id("button"));
		completesettlement.click();
		
		//WebElement cancelsettlement = driver.findElement(By.id("button"));
		//cancelsettlement.click();
		
	}

}
