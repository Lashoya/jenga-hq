package HQ;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class InvalidLogin {
	
	@Test
	public void Invalidusername() throws InterruptedException {
System.out.println("Start Home Page");
		
		//Create driver
		System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		//Open test page
		String url = "https://test.jengahq.io/#!/authenticate";
		driver.get(url);
		
		Thread.sleep(3000);
		
		//maximize browser
		driver.manage().window().maximize();
		System.out.println("Login page is opened");
		Thread.sleep(2000);
		
		//enter username
		WebElement username = driver.findElement(By.id("username"));
		username.sendKeys("lindsorobea@gmail.com");
		
		//enter password
		WebElement password = driver.findElement(By.name("password"));
		password.sendKeys("Linda@123!");
		
		Thread.sleep(1000);
		
		//click login button
		WebElement loginbutton = driver.findElement(By.tagName("button"));
		loginbutton.click();
		
		Thread.sleep(2000);
		
		//verification
		WebElement errorMessage = driver.findElement(By.id("pop up"));
		String expectedErrorMessage = "Invalid username or password";
		String actualErrorMessage = errorMessage.getText();
		
		Assert.assertTrue(actualErrorMessage.contains(expectedErrorMessage),
				"Actual Error message does not contain expected. \nActual: " + actualErrorMessage + "\nExpected: "
						+ expectedErrorMessage);
		
		Thread.sleep(2000);
		driver.quit();
	}

}
