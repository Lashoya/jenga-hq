package HQ;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class Login {
	
	private By dropdown;

	@Test
	public void login() throws InterruptedException {
		System.out.println("Start Home Page");
		
		//Create driver
		System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		//Open test page
		String url = "https://test.jengahq.io/#!/authenticate";
		driver.get(url);
		
		Thread.sleep(3000);
		
		//maximize browser
		driver.manage().window().maximize();
		System.out.println("Login page is opened");
		Thread.sleep(2000);
		
		//enter username
		WebElement username = driver.findElement(By.id("username"));
		username.sendKeys("lindasorobea@gmail.com");
		
		//enter password
		WebElement password = driver.findElement(By.name("password"));
		password.sendKeys("Linda@123!");
		
		Thread.sleep(1000);
		
		//click login button
		WebElement loginbutton = driver.findElement(By.tagName("button"));
		loginbutton.click();
		
		System.out.println("You are logged in");
		
		Thread.sleep(2000);
		
		WebElement ProfileTab = driver.findElement(By.xpath("//*[@id=\"top\"]/ul/li[3]"));
		ProfileTab.click();
		//List<WebElement> options = dropdown.findElements(By.tagName("li"));
		//System.out.println(options.size());
		// for (WebElement option : options) {
			// if (option.getText().contains("user"));
			 //System.out.println(option.getText());
			 	//option.click();
		 }
		
		
//		verifications
//		new url
//		logout button is visible
//		successful login message
		
		//close browser
		//driver.quit();
	}


