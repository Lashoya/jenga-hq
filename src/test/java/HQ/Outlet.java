package HQ;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class Outlet extends LoginDriver{
	
	@Test
	public static void Outlets() throws InterruptedException {
		System.out.println("Start PGW Settings tab");
		
	driver.manage().window().maximize();
	
	Thread.sleep(2000);
	
// Overview Tab
	WebElement settings = driver.findElement(By.xpath("/html/body/div[1]/div[3]/div[1]/div/div/ul/li[6]"));
	settings.click();
	WebElement dropdown = driver.findElement(By.xpath("/html/body/div[1]/div[3]/div[1]/div/div/ul/div[1]"));
	List<WebElement> options = dropdown.findElements(By.tagName("a"));
	System.out.println(options.size());
		for (WebElement option : options) {
			if (option.getText().contains("Outlets")) {
				System.out.println(option.getText());
					option.click();
					
	Thread.sleep(2000);
	
	WebElement outletbutton = driver.findElement(By.id("sample_editable_1_new"));
	outletbutton.click();
	//WebElement modal = driver.findElement(By.xpath("//*[@id=\"addecommerceOutletDetails\"]/div/div"));
	
	WebDriverWait wait = new WebDriverWait(driver, 100);
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[1]/div[3]/div[2]/div/div/div[4]/div/div[2]/div[2]/div[1]/div/div/div/div/div[10]/div/div/form/div/div/div/div[1]/div/div/input")));
	WebElement Name = driver.findElement(By.xpath("/html/body/div[1]/div[3]/div[2]/div/div/div[4]/div/div[2]/div[2]/div[1]/div/div/div/div/div[10]/div/div/form/div/div/div/div[1]/div/div/input"));
	Name.click();
	Name.sendKeys("German Sturat");
	
	WebElement outleturl = driver.findElement(By.xpath("//*[@id=\"addecommerceOutletDetails\"]/div/div/form/div/div/div/div[2]/div/div/input"));
	outleturl.sendKeys("https://zamra.com");
	
	WebElement email = driver.findElement(By.name("email"));
	email.sendKeys("linda.ashioya@gmail.com");
	
	WebElement mobilenumber = driver.findElement(By.xpath("/html/body/div[1]/div[3]/div[2]/div/div/div[4]/div/div[2]/div[2]/div[1]/div/div/div/div/div[10]/div/div/form/div/div/div/div[4]/div/div/div/div[2]"));
	mobilenumber.click();
	
	WebElement countrylist = driver.findElement(By.xpath("/html/body/div[1]/div[3]/div[2]/div/div/div[4]/div/div[2]/div[2]/div[1]/div/div/div/div/div[10]/div/div/form/div/div/div/div[4]/div/div/ul"));
		List<WebElement> countries = countrylist.findElements(By.tagName("li"));
		System.out.println(countries.size());
			for (WebElement country : countries) {
				if (country.getText().contains("Japan")) {
					System.out.println(country.getText());
					country.click();
	WebDriverWait wait1 = new WebDriverWait(driver, 110);
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[1]/div[3]/div[2]/div/div/div[4]/div/div[2]/div[2]/div[1]/div/div/div/div/div[10]/div/div/form/div/div/div/div[4]/div/input")));				
	WebElement mobilenumber1 = driver.findElement(By.xpath("/html/body/div[1]/div[3]/div[2]/div/div/div[4]/div/div[2]/div[2]/div[1]/div/div/div/div/div[10]/div/div/form/div/div/div/div[4]/div/input"));
	mobilenumber1.sendKeys("1-02-564258025");
	
	Thread.sleep(2000);
	
	WebElement city = driver.findElement(By.xpath("/html/body/div[1]/div[3]/div[2]/div/div/div[4]/div/div[2]/div[2]/div[1]/div/div/div/div/div[10]/div/div/form/div/div/div/div[5]/div/div/input"));
	city.sendKeys("Perth");
	
	WebElement Postalcode = driver.findElement(By.xpath("/html/body/div[1]/div[3]/div[2]/div/div/div[4]/div/div[2]/div[2]/div[1]/div/div/div/div/div[10]/div/div/form/div/div/div/div[6]/div/div/input"));
	Postalcode.sendKeys("105426");
	
	WebElement State = driver.findElement(By.xpath("/html/body/div[1]/div[3]/div[2]/div/div/div[4]/div/div[2]/div[2]/div[1]/div/div/div/div/div[10]/div/div/form/div/div/div/div[7]/div/div/input"));
	State.sendKeys("Drundum");
	
	WebElement Companyname = driver.findElement(By.xpath("/html/body/div[1]/div[3]/div[2]/div/div/div[4]/div/div[2]/div[2]/div[1]/div/div/div/div/div[10]/div/div/form/div/div/div/div[8]/div/div/input"));
	Companyname.sendKeys("Reliance Corporation");
	
	WebElement Street = driver.findElement(By.xpath("/html/body/div[1]/div[3]/div[2]/div/div/div[4]/div/div[2]/div[2]/div[1]/div/div/div/div/div[10]/div/div/form/div/div/div/div[9]/div/div/input"));
	Street.sendKeys("Street 105N");
	
	Thread.sleep(2000);
	
	Select Outletcountry = new Select(driver.findElement(By.id("Outletcountry")));
	Outletcountry.selectByValue("Japan");
	
	Thread.sleep(3000);
	
	WebElement SaveChanges = driver.findElement(By.xpath("/html/body/div[1]/div[3]/div[2]/div/div/div[4]/div/div[2]/div[2]/div[1]/div/div/div/div/div[10]/div/div/form/div/div/div/div[11]/button[2]"));
	SaveChanges.click();
	
				}
			}
			}
		}

}
}