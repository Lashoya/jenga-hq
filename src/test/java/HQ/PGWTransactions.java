package HQ;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

public class PGWTransactions extends LoginDriver{
	
	@Test
	
	public static void PGWTransactions() throws InterruptedException {
		
		System.out.println("Start Transactions tab");
		
		driver.manage().window().maximize();
		
			Thread.sleep(2000);
			
		//Click Transactions tab
		WebElement Transactions = driver.findElement(By.xpath("/html/body/div[1]/div[3]/div[1]/div/div/ul/li[4]/a"));
		Transactions.click();
		
		WebElement form = driver.findElement(By.id("searchTransactions"));
		WebElement daterow = form.findElement(By.xpath("//*[@id=\"datatable_products\"]/thead/tr[2]"));
		WebElement from = daterow.findElement(By.xpath("//*[@id=\"datePickerFrom\"]/input"));
		from.sendKeys("26/04/2020");
		WebElement to = daterow.findElement(By.xpath("//*[@id=\"datePickerTo\"]/input"));
		to.sendKeys("30/06/2020");
		Select status = new Select(daterow.findElement(By.tagName("select")));
		status.selectByValue("5");
		WebElement search = daterow.findElement(By.xpath("//*[@id=\"datatable_products\"]/thead/tr[2]/td[4]/div/button[1]"));
		search.click();
		
		System.out.println("search transactions done");
		
		//WebElement ResetFilter = driver.findElement(By.xpath("//*[@id=\"datatable_products\"]/thead/tr[2]/td[4]/div/button[2]"));
		//ResetFilter.click();
		//System.out.println("Filtering reset done");
		
		//Thread.sleep(1000);
		
		//WebElement ExportData = driver.findElement(By.xpath("/html/body/div[1]/div[3]/div[2]/div/div/div[4]/div/div/div[1]/button"));
		//ExportData.click();
		//WebElement DownloadPDF = driver.findElement(By.xpath("/html/body/div[1]/div[3]/div[2]/div/div/div[4]/div/div/div[1]/ul/li[2]/a"));
		//DownloadPDF.click();
		//WebElement DownloadExcel = driver.findElement(By.xpath("/html/body/div[1]/div[3]/div[2]/div/div/div[4]/div/div/div[1]/ul/li[3]/a"));
		//DownloadExcel.click();
		//System.out.println("file download success!");
		
	}
	

}
