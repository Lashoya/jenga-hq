package HQ;

import org.testng.annotations.Test;

public class Control {
	@Test(priority=1,enabled=true, description="login")
	public void login() throws InterruptedException {
		LoginDriver.login();
	}
	@Test(priority=3,enabled=true, description="balakdknfjvn")
	public void api() throws InterruptedException {
		APISubscriptions.APISubscription();
	}
	@Test(priority=2,enabled=true, description="Linda")
	public void pgw() throws InterruptedException {
		PGWTransactions.PGWTransactions();
	}
	@Test(priority=4,enabled=true, description="outlet")
	public void outlets() throws InterruptedException {
		Outlet.Outlets();
	}
	@Test(priority=5, enabled=true, description="IPN")
	public void ipn() throws InterruptedException {
		IPN.IPN();
	}
	@Test(priority=6) 
	public void payment() throws InterruptedException {
		Payment.Payment();
	}
	@Test(priority=7)
	public void overview() throws InterruptedException {
		PGWOverview.Overview();
	}
	@Test(priority=8)
	public void APIKeys() throws InterruptedException {
		APIKeys.APIKeys();
	}
	@Test(priority=9)
	public void GOLive() throws InterruptedException {
		GOLive.GOLive();
	}
	@Test(priority=10)
	public void batchtransactions() throws InterruptedException {
		BatchTransactions.BatchTransactions();
	}
	@Test(priority=11)
	public void batchoverview() throws InterruptedException {
		BatchOverview.BatchOverview();
	}
	@Test(priority=12)
	public void accountoverview() throws InterruptedException {
		AccountSettings.Overview();
	}
	@Test(priority=13)
	public void support() throws InterruptedException {
		Support.Support();
	}
	@Test(priority=14)
	public void Homepage() throws InterruptedException {
		HQ.Homepage.Homepage();
	
	}
	@Test(priority=15)
	public void logout() throws InterruptedException {
		Logout.Logout();
	}

}

